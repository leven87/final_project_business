package com.example.demo.system;

import java.math.BigDecimal;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.core.Response;

public interface IDTUPay {

    /**
     * request from token manager
     * 
     * @param cprNumber user's CPR
     * @param i the number of tokens requested
     * @return a list of tokens
     */
    public List<Token> requestTokens(String cprNumber, int i);

    /**
     * register Merchant in user manager
     * 
     * @param merchantCPR merchant's CPR
     * @param firstName first name
     * @param lastName  last name 
     * @param account bank account
     */
    
    public void registerMerchant(String merchantCPR, String firstName, String lastName, String account);
    
    /**
     * register customer in user manager
     * 
     * @param customerCPR user's CPR
     * @param firstName first name
     * @param lastName  last name
     * @param account bank account
     */
    public void registerCustomer(String customerCPR, String firstName, String lastName, String account);
    
    /**
     * request payment from the bank
     * 
     * @param token customer's token
     * @param customerAccount customer account
     * @param merchantAccount  merchant account
     * @param amount the amount of transfer money
     * @return the result of the payment
     */
    public Response payWithToken(Token token, String customerAccount, String merchantAccount, BigDecimal amount);
}
