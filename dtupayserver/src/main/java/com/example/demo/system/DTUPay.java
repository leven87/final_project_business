package com.example.demo.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;


import com.example.demo.rest.dto.*;

/**
 * dtupay center
 * 
 * have some business logic of dtupay
 * 
 * @author Li Wang, Zihao Li, Bijan, Stuart
 *
 */

public class DTUPay implements IDTUPay{
    
    BankService bank;
    WebTarget userManagerUrl, tokenManagerUrl, paymentManagerUrl;
    private String tokenServicePath = "TokenService";
    
/**
 * Initialization function
 * 
 * initialize clients and bank
 * @param bankService
 */

    public DTUPay(BankService bankService) {
        this.bank = bankService;
        Client userManagerClient = ClientBuilder.newClient();
        Client tokenManagerClient = ClientBuilder.newClient();
        Client paymentManagerClient = ClientBuilder.newClient();
        userManagerUrl = userManagerClient.target("http://fastmoney-23.compute.dtu.dk:8085/");
        tokenManagerUrl = tokenManagerClient.target("http://fastmoney-23.compute.dtu.dk:8080/");
        paymentManagerUrl = paymentManagerClient.target("http://fastmoney-23.compute.dtu.dk:8881/");

    }

    /**
     * register customer in user manager
     * 
     * @param customerCPR user's CPR
     * @param firstName first name
     * @param lastName  last name
     * @param account bank account
     */    
    public void registerCustomer(String customerCPR, String firstName, String lastName, String account) {
    	
		DtuPayUserRepresentation customer = new DtuPayUserRepresentation();
		customer.setCpr(customerCPR);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setAccount(account);
		userManagerUrl.path("/")
		.path("customers")
		.request()
		.post(Entity.entity(customer, "application/json"));    	
    }

    /**
     * register Merchant in user manager
     * 
     * @param merchantCPR merchant's CPR
     * @param firstName first name
     * @param lastName  last name 
     * @param account bank account
     */    
    public void registerMerchant(String merchantCPR, String firstName, String lastName, String account) {
		DtuPayUserRepresentation merchant = new DtuPayUserRepresentation();
		merchant.setCpr(merchantCPR);
		merchant.setFirstName(firstName);
		merchant.setLastName(lastName);
		merchant.setAccount(account);
		userManagerUrl.path("/")
		.path("merchants")
		.request()
		.post(Entity.entity(merchant, "application/json"));
    }
/**
 * mark token used in token manager
 *     
 * @param token user's token
 */
	public void markTokenUsed(Token token) {		
		tokenManagerUrl.path(tokenServicePath)
		.path(String.format("tokens/%s", token.getTokenId()))
		.request()
		.post(Entity.entity(token.getTokenId(), "application/json"));
		
	}

    /**
     * request from token manager
     * 
     * @param cprNumber user's CPR
     * @param i the number of tokens requested
     * @return a list of tokens
     */	
	public List<Token> requestTokens(String cprNumber, int i) {		
		TokenRequest tr = new TokenRequest();
		tr.setCpr(cprNumber);
		tr.setNumber(i);
		
		
		Token[] tokens = tokenManagerUrl.path(tokenServicePath)
    		.path("tokens")
    		.request()
    		.accept(MediaType.APPLICATION_JSON)
    		.post(Entity.entity(tr, "application/json"), Token[].class);
		
		return Arrays.asList(tokens);
	}

/**
 * get token status from token manager
 * 	
 * @param token token
 * @return true or false
 */
	public boolean getTokenStatus(Token token) {        
		boolean result =
				tokenManagerUrl.path(tokenServicePath)
		    		.path(String.format("tokens/%s", token.getTokenId()))
		    		.request()
		    		.accept(MediaType.TEXT_PLAIN)
		    		.get(Boolean.class);
		        
		        return result;  
	}



    /**
     * request payment from the bank
     * 
     * @param token customer's token
     * @param customerAccount customer account
     * @param merchantAccount  merchant account
     * @param amount the amount of transfer money
     * @return the result of the payment
     */    
    public Response payWithToken(Token token, String customerAccount, String merchantAccount, BigDecimal amount) {
    	String description = "PaymentRequested";
        if(!getTokenStatus(token)) {
		Response response = requestPayment(customerAccount, merchantAccount, amount, description);
		markTokenUsed(token);
		return response;
		}
		else return null;
    }
    
    
	public Response requestPayment(String account1, String account2, BigDecimal i, String comment) {
	     PaymentServiceRequest pr = new PaymentServiceRequest(account1,account2,i, comment);
	     Response response = paymentManagerUrl.path("payment").request().post(Entity.json(pr),Response.class);	     
	     return response;
	}
    
}
