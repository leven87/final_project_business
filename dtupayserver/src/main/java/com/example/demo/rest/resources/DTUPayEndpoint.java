package com.example.demo.rest.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import com.example.demo.system.*;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;


import com.example.demo.rest.dto.*;

/**
 * 
 * @author Li Wang, Zihao Li
 *
 */

@Path("/dtupay")
public class DTUPayEndpoint {
	/* A singleton of dtupay */
	private static DTUPay dtuPay = new DTUPay(new BankServiceService().getBankServicePort());
	
	@GET
	@Path("test")
	@Produces("text/plain")
	public String test() {
		
		return "yo yo yo";
	}

/**
 * register customer in dtuPay
 * 	
 * @param c
 * @return customer cpr
 */
	@POST
	@Path("customers")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerCustomer(DtuPayUserRepresentation c) {
		dtuPay.registerCustomer(c.getCpr()
				, c.getFirstName()
				, c.getLastName()
				, c.getAccount());
		return c.getCpr();
	}

/**
 * register merchant in dtuPay
 * 	
 * @param m
 * @return merchant CPR 
 */
	@POST
	@Path("merchants")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerMerchant(DtuPayUserRepresentation m) {
		dtuPay.registerMerchant(m.getCpr()
				, m.getFirstName()
				, m.getLastName()
				, m.getAccount());
		return m.getCpr();
	}

/**
 * request token 
 * 
 * @param r
 * @return a list of tokens
 */
	@POST
	@Path("tokenRequest")
	@Consumes("application/json")
	@Produces("application/json")
	public List<Token> requestTokens(TokenRequest r) {
		return dtuPay.requestTokens(r.getCpr(), r.getNumber());
	}
	
/**
 * transfer money with token interface
 * 	
 * @param p payment request 
 * @return response information
 * @throws BankServiceException_Exception
 */
	@POST
	@Path("pay")
	@Consumes("application/json")
	public Response payWithToken(PaymentRequest p)  {
//			if (response==null) {
//			return Response.noContent().build();
//			} else {
//				return Response.status(Response.Status.BAD_REQUEST).build();
//			}
//		}
		return dtuPay.payWithToken(p.getToken()
				, p.getCustomerAcct()
				, p.getMerchantAcct()	
				, p.getAmount());
	}
}

	