package com.example.demo.rest.dto;

import java.math.BigDecimal;

public class PaymentServiceRequest {
	
	public String debtor;
	public String creditor;
	public BigDecimal amount;
	public String comment;
	
	public PaymentServiceRequest() {};
	
	public PaymentServiceRequest(String account1, String account2, BigDecimal i,String comment) {
		debtor = account1;
		creditor = account2;
		amount = i;
		this.comment = comment;
	}

}
